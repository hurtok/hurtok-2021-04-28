from ivansort import ivansort

from datetime import date


def is_sorted(arr, key=None):
    if key is None:
        key = lambda x: x
    prev_element = arr[0]
    for curr_element in arr[1:]:
        if key(curr_element) < key(prev_element):
            return False
        prev_element = curr_element
    return True


def test_is_result_sorted():
    arr = [1, 3, 2]
    maybe_sorted = ivansort(arr)
    assert is_sorted(maybe_sorted)


def test_expected_result():
    arr = [1, 3, 2]
    assert ivansort(arr) == [1, 2, 3]


class Article:
    def __init__(self, author, title, pub_date) -> None:
        self.author = author
        self.title = title
        self.pub_date = pub_date


def get_pub_date(article: Article):
    return article.pub_date


def test_sort_articles_by_pub_date():
    articles = [
        Article("Ivan", "Sorting data", date(2021, 4, 28)),
        Article("Olesia", "Design of business cards", date(2021, 1, 18)),
        Article(
            "Dmytro Valentynovych Kyrychuk", "Python for beginners", date(2023, 7, 23)
        ),
    ]

    assert is_sorted(ivansort(articles, key=get_pub_date), key=get_pub_date)
